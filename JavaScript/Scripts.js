/*let spades2 = [2, 'Two of Spades'];
let spades3 = [3, 'Three of Spades'];
let spades4 = [4, 'Four of Spades'];
let spades5 = [5, 'Five of Spades'];
let spades6 = [6, 'Six of Spades'];
let spades7 = [7, 'Seven of Spades'];
let spades8 = [8, 'Eight of Spades'];
let spades9 = [9, 'Nine of Spades'];
let spades10 = [10, 'Ten of Spades'];
let jackSpades = [10, 'Jack of Spades'];
let queenSpades = [10, 'Queen of Spades'];
let kingSpades = [10, 'King of Spades'];
let aceSpades = [11, 'Ace of Spades'];
let hearts2 = [2, 'Two of Hearts'];
let hearts3 = [3, 'Three of Hearts'];
let hearts4 = [4, 'Four of Hearts'];
let hearts5 = [5, 'Five of Hearts'];
let hearts6 = [6, 'Six of Hearts'];
let hearts7 = [7, 'Seven of Hearts'];
let hearts8 = [8, 'Eight of Hearts'];
let hearts9 = [9, 'Nine of Hearts'];
let hearts10 = [10, 'Ten of Hearts'];
let jackHearts = [10, 'Jack of Hearts'];
let queenHearts = [10, 'Queen of Hearts'];
let kingHearts = [10, 'King of Hearts'];
let aceHearts = [11, 'Ace of Hearts'];
let clubs2 = [2, 'Two of Clubs'];
let clubs3 = [3, 'Three of Clubs'];
let clubs4 = [4, 'Four of Clubs'];
let clubs5 = [5, 'Five of Clubs'];
let clubs6 = [6, 'Six of Clubs'];
let clubs7 = [7, 'Seven of Clubs'];
let clubs8 = [8, 'Eight of Clubs'];
let clubs9 = [9, 'Nine of Clubs'];
let clubs10 = [10, 'Ten of Clubs'];
let jackClubs = [10, 'Jack of Clubs'];
let queenClubs = [10, 'Queen of Clubs'];
let kingClubs = [10, 'King of Clubs'];
let aceClubs = [11, 'Ace of Clubs'];
let diamonds2 = [2, 'Two of Diamonds'];
let diamonds3 = [3, 'Three of Diamonds'];
let diamonds4 = [4, 'Four of Diamonds'];
let diamonds5 = [5, 'Five of Diamonds'];
let diamonds6 = [6, 'Six of Diamonds'];
let diamonds7 = [7, 'Seven of Diamonds'];
let diamonds8 = [8, 'Eight of Diamonds'];
let diamonds9 = [9, 'Nine of Diamonds'];
let diamonds10 = [10, 'Ten of Diamonds'];
let jackDiamonds = [10, 'Jack of Diamonds'];
let queenDiamonds = [10, 'Queen of Diamonds'];
let kingDiamonds = [10, 'King of Diamonds'];
let aceDiamonds = [11, 'Ace of Diamonds'];*/
const hit = document.getElementById('hit');
const stand = document.getElementById('stand');
const replay = document.getElementById('replay');
const deal = document.getElementById('deal');
const blackjack = document.getElementById('Blackjack');
const winner = document.getElementById('winner');
const playerDrawnCards = document.getElementById('playerDrawnCards');
const playerCardsVal = document.getElementById('playerCardsVal');
const dealerDrawnCards = document.getElementById('dealerDrawnCards');
const dealerCardsVal = document.getElementById('dealerCardsVal');

const suites = ['Spades', 'Hearts', 'Clubs', 'Diamonds'];
const values = [2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10, 11]
const names = ['Two of ', 'Three of ', 'Four of ', 'Five of ', 'Six of ', 'Seven of ', 'Eight of ', 'Nine of ', 'Ten of ', 'Jack of ', 'Queen of ', 'King of ', 'Ace of ']
class Card {
    constructor (value, name) {
        this.value = value;
        this.name = name;
    }
  }
const deckOfCards = [];
/*let cardsSpades = [spades2, spades3, spades4, spades5, spades6, spades7, spades8, spades9, spades10, jackSpades, queenSpades, kingSpades, aceSpades];
let cardsHearts = [hearts2, hearts3, hearts4, hearts5, hearts6, hearts7, hearts8, hearts9, hearts10, jackHearts, queenHearts, kingHearts, aceHearts];
let cardsClubs = [clubs2, clubs3, clubs4, clubs5, clubs6, clubs7, clubs8, clubs9, clubs10, jackClubs, queenClubs, kingClubs, aceClubs];
let cardsDiamonds = [diamonds2, diamonds3, diamonds4, diamonds5, diamonds6, diamonds7, diamonds8, diamonds9, diamonds10, jackDiamonds, queenDiamonds, kingDiamonds, aceDiamonds];
let deckOfCards = [cardsSpades, cardsHearts, cardsClubs, cardsDiamonds];*/
let drawnCardsPlayer = [];
let drawnCardsNamePlayer = [];
let totalCardValPlayer;
let drawnCardsDealer = [];
let drawnCardsValDealer = [];
let drawnCardsNameDealer = [];
let totalCardValDealer;
let playerDrawHelp1;
//let playerDrawHelp2;
let dealerDrawHelp1;
//let dealerDrawHelp2;
hit.disabled = true;
stand.disabled = true;
replay.disabled = true;

for (let x = 0; x < 4; x++) {
    for (let j = 0; j < 13; j++) {
        deckOfCards.push(Object.values(new Card(values[j], names[j] + suites[x])));
    }
  }

function start() {
    hit.disabled = false;
    stand.disabled = false;
    deal.disabled = true;

    for (let x = 0; x < 4; x++) {
        for (let j = 0; j < 13; j++) {
            deckOfCards.push(Object.values(new Card(values[j], names[j] + suites[x])));
        }
      }
      
    console.log(deckOfCards);
    
    for (let i = 0; i < 2; i++) {
        playerDrawHelp1 = Math.floor(Math.random() * deckOfCards.length);
        //playerDrawHelp2 = Math.floor(Math.random() * cardsSpades.length);
        drawnCardsPlayer.push(deckOfCards[playerDrawHelp1]);
        drawnCardsNamePlayer.push(deckOfCards[playerDrawHelp1][1]);
        totalCardValPlayer = drawnCardsPlayer.reduce((a, b) => a + b[0],0);
        deckOfCards.splice(playerDrawHelp1);
        if (totalCardValPlayer == 21 && totalCardValDealer != 21) {
            blackjack.innerHTML = 'Blackjack!';
            winner.innerHTML = 'Player is the winner!';
            hit.disabled = true;
            stand.disabled = true;
            replay.disabled = false;
        }

        dealerDrawHelp1 = Math.floor(Math.random() * deckOfCards.length);
        //dealerDrawHelp2 = Math.floor(Math.random() * cardsSpades.length);
        drawnCardsDealer.push(deckOfCards[dealerDrawHelp1]);
        drawnCardsValDealer.push(deckOfCards[dealerDrawHelp1][0]);
        drawnCardsNameDealer.push(deckOfCards[dealerDrawHelp1][1]);
        totalCardValDealer = drawnCardsDealer.reduce((a, b) => a + b[0],0);
        deckOfCards.splice(dealerDrawHelp1);
        if (totalCardValDealer == 21) {
            blackjack.innerHTML = 'Blackjack!';
            winner.innerHTML = 'Dealer is the winner!';
            hit.disabled = true;
            stand.disabled = true;
            replay.disabled = false;
        }
    }
    playerDrawnCards.innerHTML = drawnCardsNamePlayer;
    playerCardsVal.innerHTML = totalCardValPlayer;
    dealerDrawnCards.innerHTML = drawnCardsNameDealer[0];
    dealerCardsVal.innerHTML = drawnCardsValDealer[0];
}
function drawCard() {
    playerDrawHelp1 = Math.floor(Math.random() * deckOfCards.length);
    //playerDrawHelp2 = Math.floor(Math.random() * cardsSpades.length);
    drawnCardsPlayer.push(deckOfCards[playerDrawHelp1]);
    drawnCardsNamePlayer.push(deckOfCards[playerDrawHelp1][1]);
    totalCardValPlayer = drawnCardsPlayer.reduce((a, b) => a + b[0],0);
    deckOfCards.splice(playerDrawHelp1);
    playerDrawnCards.innerHTML = drawnCardsNamePlayer;
    if (totalCardValPlayer == 21) {
        blackjack.innerHTML = 'Blackjack!';
        winner.innerHTML = 'Player is the winner!';
        hit.disabled = true;
        stand.disabled = true;
        replay.disabled = false;
    }
    if (totalCardValPlayer < 22) {
        playerCardsVal.innerHTML = totalCardValPlayer;
    } else {
        aceSpades[0] = 1;
        totalCardValPlayer = drawnCardsPlayer.reduce((a, b) => a + b[0],0);
        if (totalCardValPlayer < 22) {
            playerCardsVal.innerHTML = totalCardValPlayer;
        } else {
            aceHearts[0] = 1;
            totalCardValPlayer = drawnCardsPlayer.reduce((a, b) => a + b[0],0);
            if (totalCardValPlayer < 22) {
                playerCardsVal.innerHTML = totalCardValPlayer;
            } else {
                aceClubs[0] = 1;
                totalCardValPlayer = drawnCardsPlayer.reduce((a, b) => a + b[0],0);
                if (totalCardValPlayer < 22) {
                    playerCardsVal.innerHTML = totalCardValPlayer;
                } else {
                    aceDiamonds[0] = 1;
                    totalCardValPlayer = drawnCardsPlayer.reduce((a, b) => a + b[0],0);
                    if (totalCardValPlayer < 22) {
                        playerCardsVal.innerHTML = totalCardValPlayer;
                    } else {
                        playerCardsVal.innerHTML = totalCardValPlayer + ' DEAD';
                        hit.disabled = true;
                        stand.disabled = true;
                        replay.disabled = false;
                    }
                }
            }
        }
    }
}
function drawCardsDealer() {
    hit.disabled = true;
    stand.disabled = true;
    replay.disabled = false;
    if (totalCardValPlayer == 16) {
        while (totalCardValDealer < 16) {
            dealerDrawHelp1 = Math.floor(Math.random() * deckOfCards.length);
            //dealerDrawHelp2 = Math.floor(Math.random() * cardsSpades.length);
            drawnCardsDealer.push(deckOfCards[dealerDrawHelp1]);
            drawnCardsNameDealer.push(deckOfCards[dealerDrawHelp1][1]);
            totalCardValDealer = drawnCardsDealer.reduce((a, b) => a + b[0],0);
            deckOfCards.splice(dealerDrawHelp1);
            if (totalCardValDealer < 22) {
                dealerCardsVal.innerHTML = totalCardValDealer;
            } else {
                aceSpades[0] = 1;
                totalCardValDealer = drawnCardsDealer.reduce((a, b) => a + b[0],0);
                if (totalCardValDealer < 22) {
                    dealerCardsVal.innerHTML = totalCardValDealer;
                } else {
                    aceHearts[0] = 1;
                    totalCardValDealer = drawnCardsDealer.reduce((a, b) => a + b[0],0);
                    if (totalCardValDealer < 22) {
                        dealerCardsVal.innerHTML = totalCardValDealer;
                    } else {
                        aceClubs[0] = 1;
                        totalCardValDealer = drawnCardsDealer.reduce((a, b) => a + b[0],0);
                        if (totalCardValDealer < 22) {
                            dealerCardsVal.innerHTML = totalCardValDealer;
                        } else {
                            aceDiamonds[0] = 1;
                            totalCardValDealer = drawnCardsDealer.reduce((a, b) => a + b[0],0);
                            if (totalCardValDealer < 22) {
                                dealerCardsVal.innerHTML = totalCardValDealer;
                            }
                        }
                    }
                }
            }
        }
            if (totalCardValDealer == 21) {
                blackjack.innerHTML = 'Blackjack!';
                winner.innerHTML = 'Dealer is the winner!';
            }  
        } else {
        while (totalCardValDealer < 17) {
            dealerDrawHelp1 = Math.floor(Math.random() * deckOfCards.length);
            //dealerDrawHelp2 = Math.floor(Math.random() * cardsSpades.length);
            drawnCardsDealer.push(deckOfCards[dealerDrawHelp1]);
            drawnCardsNameDealer.push(deckOfCards[dealerDrawHelp1][1]);
            totalCardValDealer = drawnCardsDealer.reduce((a, b) => a + b[0],0);
            deckOfCards.splice(dealerDrawHelp1);
            if (totalCardValDealer < 22) {
                dealerCardsVal.innerHTML = totalCardValDealer;
            } else {
                aceSpades[0] = 1;
                totalCardValDealer = drawnCardsDealer.reduce((a, b) => a + b[0],0);
                if (totalCardValDealer < 22) {
                    dealerCardsVal.innerHTML = totalCardValDealer;
                } else {
                    aceHearts[0] = 1;
                    totalCardValDealer = drawnCardsDealer.reduce((a, b) => a + b[0],0);
                    if (totalCardValDealer < 22) {
                        dealerCardsVal.innerHTML = totalCardValDealer;
                    } else {
                        aceClubs[0] = 1;
                        totalCardValDealer = drawnCardsDealer.reduce((a, b) => a + b[0],0);
                        if (totalCardValDealer < 22) {
                            dealerCardsVal.innerHTML = totalCardValDealer;
                        } else {
                            aceDiamonds[0] = 1;
                            totalCardValDealer = drawnCardsDealer.reduce((a, b) => a + b[0],0);
                            if (totalCardValDealer < 22) {
                                dealerCardsVal.innerHTML = totalCardValDealer;
                            }
                        }
                    }
                }
            }
        }
            if (totalCardValDealer == 21) {
                blackjack.innerHTML = 'Blackjack!';
                winner.innerHTML = 'Dealer is the winner!';
            } 
        }
    dealerDrawnCards.innerHTML = drawnCardsNameDealer;
    dealerCardsVal.innerHTML = totalCardValDealer;
    if (totalCardValDealer > 21) {
        dealerCardsVal.innerHTML = totalCardValDealer + ' DEAD!';
            winner.innerHTML = 'Player is the winner!';
    } else if (totalCardValDealer < totalCardValPlayer) {
        winner.innerHTML = 'Player is the winner!';
    } else {
        winner.innerHTML = 'Dealer is the winner!';
    }
}
function playAgain() {
    deal.disabled = false;
    replay.disabled = true;
    drawnCardsPlayer = [];
    drawnCardsNamePlayer = [];
    drawnCardsDealer = [];
    drawnCardsValDealer = [];
    drawnCardsNameDealer = [];
    /*cardsSpades = [spades2, spades3, spades4, spades5, spades6, spades7, spades8, spades9, spades10, jackSpades, queenSpades, kingSpades, aceSpades];
    cardsHearts = [hearts2, hearts3, hearts4, hearts5, hearts6, hearts7, hearts8, hearts9, hearts10, jackHearts, queenHearts, kingHearts, aceHearts];
    cardsClubs = [clubs2, clubs3, clubs4, clubs5, clubs6, clubs7, clubs8, clubs9, clubs10, jackClubs, queenClubs, kingClubs, aceClubs];
    cardsDiamonds = [diamonds2, diamonds3, diamonds4, diamonds5, diamonds6, diamonds7, diamonds8, diamonds9, diamonds10, jackDiamonds, queenDiamonds, kingDiamonds, aceDiamonds];
    deckOfCards = [cardsSpades, cardsHearts, cardsClubs, cardsDiamonds];*/
    playerDrawnCards.innerHTML = '';
    playerCardsVal.innerHTML = '';
    dealerDrawnCards.innerHTML = '';
    dealerCardsVal.innerHTML = '';
    blackjack.innerHTML = '';
    winner.innerHTML = '';
}
